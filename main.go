package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"gitlab.com/valdemar-ceccon/puzzle-solver/solver"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("usage: pass a csv with puzzles to solve")
		os.Exit(0)
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		fmt.Println("can't open file: ", err)
		os.Exit(1)
	}

	content, err := ioutil.ReadAll(f)
	if err != nil {
		fmt.Println("can't read the file: ", err)
		os.Exit(1)
	}

	text := strings.Split(string(content), "\n")

	for _, l := range text {
		puzzle := strings.Split(l, ",")
		if len(puzzle) == 1 {
			notSolved, err := stringToPuzzle(puzzle[0])
			if err != nil {
				fmt.Println("Puzzle cannot be solved: ", err)
				continue
			}

			solved, err := solver.Solve(notSolved)
			if err != nil {
				fmt.Println("Puzzle cannot be solved: ", err)
				continue
			}

			fmt.Printf("Solution: %v\n", solved)
		} else if len(puzzle) == 2 {
			notSolved, err := stringToPuzzle(puzzle[0])
			givenSolved, err := stringToPuzzle(puzzle[1])

			if err != nil {
				fmt.Println("Puzzle cannot be solved: ", err)
				continue
			}

			solved, err := solver.Solve(notSolved)
			if err != nil {
				fmt.Println("Puzzle cannot be solved: ", err)
				continue
			}
			if !compareSolutions(solved, givenSolved) {
				fmt.Printf("Puzzle: %v", notSolved)

				fmt.Printf("Solution: %v\nGiven Solution: %v", solved, givenSolved)
			}

		}

	}
}

func compareSolutions(a *[9][9]uint8, b *[9][9]uint8) bool {
	if a == b {
		return true
	}

	if a == nil && b == nil {
		return true
	}

	if len(a) != len(b) {
		return false
	}

	for idx := range a {
		if len(a[idx]) != len(b[idx]) {
			return false
		}

		if a[idx] != b[idx] {
			return false
		}
	}

	return true
}

func stringToPuzzle(p string) (*[9][9]uint8, error) {
	if len(p) != 81 {
		return nil, errors.New("Puzzle is not valid:" + p)
	}
	ret := &[9][9]uint8{}
	for idx, r := range p {
		i, err := strconv.Atoi(string(r))
		if err != nil {
			return nil, fmt.Errorf("Charachter %s not valid in puzzle is not valid: %v", string(r), p)
		}

		ret[idx/9][idx%9] = uint8(i)
	}
	return ret, nil
}
