package solver

import "errors"

// Solve find missing values in the puzzle, if possible
func Solve(s *[9][9]uint8) (*[9][9]uint8, error) {
	if completed(s) {
		return s, nil
	}

	x, y := nextEmpty(s)

	ns := clone(s)
	var n uint8 = 1
	var err error
	var ret *[9][9]uint8
	for n <= 9 {
		ns[x][y] = n
		if allValid(ns) {
			ret, err = Solve(ns)
			if ret != nil && completed(ret) && allValid(ns) {
				return ret, err
			}
		}

		n++
	}

	return nil, errors.New("Puzzle not valid")
}

func clone(s *[9][9]uint8) *[9][9]uint8 {
	ret := &[9][9]uint8{}

	for ir, r := range s {
		for ic, c := range r {
			ret[ir][ic] = c
		}
	}

	return ret
}

func nextEmpty(s *[9][9]uint8) (int, int) {
	for ir, r := range s {
		for ic, c := range r {
			if c == 0 {
				return ir, ic
			}
		}
	}

	return -1, -1
}

func completed(s *[9][9]uint8) bool {
	for _, r := range s {
		for _, c := range r {
			if c == 0 {
				return false
			}
		}
	}

	return true
}

func testValues(values [9]uint8) bool {
	seen := make(map[uint8]bool)

	for _, c := range values {
		if _, ok := seen[c]; ok {
			return false
		}

		if c != 0 {
			seen[c] = true
		}

	}

	return true
}

func testLines(s *[9][9]uint8) bool {
	for _, l := range s {
		if !testValues(l) {
			return false
		}
	}

	return true
}

func testColumns(s *[9][9]uint8) bool {
	var col [9]uint8
	for cidx := 0; cidx < 9; cidx++ {
		for idx, line := range s {
			col[idx] = line[cidx]
		}
		if !testValues(col) {
			return false
		}
	}

	return true
}

func block(s *[9][9]uint8, n int) [9]uint8 {
	var ret [9]uint8
	br, bc := (n/3)*3, (n%3)*3
	for ri, r := range s[br : br+3] {
		for ci, c := range r[bc : bc+3] {
			ret[ri*3+ci] = c
		}
	}
	return ret
}

func testBlocks(s *[9][9]uint8) bool {
	for n := 0; n < 9; n++ {
		if !testValues(block(s, n)) {
			return false
		}
	}

	return true
}

func allValid(s *[9][9]uint8) bool {
	return testLines(s) && testColumns(s) && testBlocks(s)
}
