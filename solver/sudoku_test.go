package solver

import "testing"

func TestNewSudoku(t *testing.T) {
	want := &[9][9]uint8{
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 0, 0, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
	}

	got := clone(want)

	for il, l := range got {
		for ic, c := range l {
			if want[il][ic] != c {
				t.Error("wrong puzzle")
			}
		}
	}
}

func TestTestValues(t *testing.T) {
	given := &[9][9]uint8{
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{9, 8, 7, 0, 0, 4, 3, 2, 1},
		{2, 2, 3, 4, 5, 6, 7, 8, 9},
	}

	testsCases := []struct {
		at   int
		want bool
	}{
		{0, true},
		{1, true},
		{2, false},
	}

	for _, c := range testsCases {
		got := testValues(given[c.at])
		if c.want != got {
			t.Errorf("linhe should return %v for testvalue(%v), but returned %v", c.want, given[c.at], got)
		}
	}
}

func TestTestLines(t *testing.T) {
	given := &[9][9]uint8{
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{9, 8, 7, 0, 0, 4, 3, 2, 1},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
	}

	valid := testLines(given)

	if !valid {
		t.Error("Should be valid")
	}

	given = &[9][9]uint8{
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{9, 8, 7, 0, 0, 4, 3, 2, 1},
		{2, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 5, 6, 7, 8, 9},
	}

	valid = testLines(given)

	if valid {
		t.Error("Should be invalidvalid")
	}
}

func TestTestColumns(t *testing.T) {
	given := &[9][9]uint8{
		{1, 1, 1, 1, 1, 1, 1, 1, 1},
		{2, 2, 2, 2, 2, 2, 2, 2, 2},
		{3, 3, 3, 3, 3, 3, 3, 3, 3},
		{4, 4, 4, 4, 4, 4, 4, 4, 4},
		{5, 5, 0, 5, 5, 5, 5, 5, 5},
		{6, 6, 6, 6, 6, 6, 6, 6, 6},
		{7, 7, 7, 7, 0, 7, 7, 7, 7},
		{8, 8, 8, 8, 8, 8, 8, 8, 8},
		{9, 9, 9, 9, 9, 9, 9, 9, 9},
	}

	valid := testColumns(given)

	if !valid {
		t.Error("Should be valid")
	}

	given = &[9][9]uint8{
		{1, 1, 1, 1, 1, 1, 1, 1, 1},
		{2, 2, 2, 2, 2, 2, 2, 2, 2},
		{3, 3, 3, 3, 3, 3, 3, 3, 3},
		{4, 4, 4, 4, 4, 5, 4, 4, 4},
		{5, 5, 0, 5, 5, 5, 5, 5, 5},
		{6, 6, 6, 6, 6, 6, 6, 6, 6},
		{7, 7, 7, 7, 0, 7, 7, 7, 7},
		{8, 8, 8, 8, 8, 8, 8, 8, 8},
		{9, 9, 9, 9, 9, 9, 9, 9, 9},
	}

	valid = testColumns(given)

	if valid {
		t.Error("Should be invalid")
	}
}

func TestTestBlocks(t *testing.T) {
	given := &[9][9]uint8{
		{1, 2, 3, 1, 2, 3, 1, 2, 3},
		{4, 5, 6, 4, 5, 6, 4, 5, 6},
		{7, 8, 9, 7, 8, 9, 7, 8, 9},
		{1, 2, 3, 1, 2, 3, 1, 2, 3},
		{4, 5, 6, 0, 5, 6, 4, 5, 6},
		{7, 8, 9, 7, 8, 9, 7, 8, 9},
		{1, 2, 3, 1, 0, 3, 1, 2, 3},
		{4, 5, 6, 4, 5, 6, 4, 5, 6},
		{7, 8, 9, 7, 8, 9, 7, 8, 9},
	}

	valid := testBlocks(given)

	if !valid {
		t.Error("Should be valid")
	}

	given = &[9][9]uint8{
		{1, 2, 3, 1, 2, 3, 1, 2, 3},
		{4, 1, 6, 4, 5, 6, 4, 5, 6},
		{7, 8, 9, 7, 8, 9, 7, 8, 9},
		{1, 2, 3, 1, 2, 3, 1, 2, 3},
		{4, 5, 6, 0, 5, 6, 4, 5, 6},
		{7, 8, 9, 7, 8, 9, 7, 8, 9},
		{1, 2, 3, 1, 0, 3, 1, 2, 3},
		{4, 5, 6, 4, 5, 6, 4, 5, 6},
		{7, 8, 9, 7, 8, 9, 7, 8, 9},
	}

	valid = testBlocks(given)

	if valid {
		t.Error("Should be invalid")
	}
}

func TestNextBlockIdx(t *testing.T) {
	given := &[9][9]uint8{
		{1, 2, 3, 1, 2, 3, 1, 2, 3},
		{4, 1, 6, 4, 5, 6, 4, 5, 6},
		{7, 8, 9, 7, 8, 9, 7, 8, 9},
		{1, 2, 3, 1, 2, 3, 1, 2, 3},
		{4, 5, 6, 0, 5, 6, 4, 5, 6},
		{7, 8, 9, 7, 8, 9, 7, 8, 9},
		{1, 2, 3, 1, 0, 3, 1, 2, 3},
		{4, 5, 6, 4, 5, 6, 4, 5, 6},
		{7, 8, 9, 7, 8, 9, 7, 8, 8},
	}

	cases := []struct {
		when int
		then [9]uint8
	}{
		{
			when: 0,
			then: [9]uint8{1, 2, 3, 4, 1, 6, 7, 8, 9},
		},
		{
			when: 1,
			then: [9]uint8{1, 2, 3, 4, 5, 6, 7, 8, 9},
		},
		{
			when: 4,
			then: [9]uint8{1, 2, 3, 0, 5, 6, 7, 8, 9},
		},
		{
			when: 8,
			then: [9]uint8{1, 2, 3, 4, 5, 6, 7, 8, 8},
		},
	}

	for _, c := range cases {
		got := block(given, c.when)
		if !sameArray(got, c.then) {
			t.Errorf("Wrong block. want %v, got %v", c.then, got)
		}
	}
}

func sameArray(a, b [9]uint8) bool {
	if len(a) != len(b) {
		return false
	}

	for idx := range a {
		if a[idx] != b[idx] {
			return false
		}
	}

	return true
}

func TestSolve(t *testing.T) {
	solved := &[9][9]uint8{
		{8, 6, 4, 3, 7, 1, 2, 5, 9},
		{3, 2, 5, 8, 4, 9, 7, 6, 1},
		{9, 7, 1, 2, 6, 5, 8, 4, 3},
		{4, 3, 6, 1, 9, 2, 5, 8, 7},
		{1, 9, 8, 6, 5, 7, 4, 3, 2},
		{2, 5, 7, 4, 8, 3, 9, 1, 6},
		{6, 8, 9, 7, 3, 4, 1, 2, 5},
		{7, 1, 3, 5, 2, 8, 6, 9, 4},
		{5, 4, 2, 9, 1, 6, 3, 7, 8},
	}

	notSolved1 := &[9][9]uint8{
		{8, 6, 4, 3, 7, 1, 2, 5, 9},
		{3, 2, 5, 8, 4, 9, 7, 6, 1},
		{9, 7, 1, 2, 6, 5, 8, 4, 3},
		{4, 3, 6, 1, 0, 2, 5, 8, 7},
		{1, 9, 8, 6, 5, 7, 4, 3, 2},
		{2, 5, 7, 4, 8, 3, 9, 1, 6},
		{6, 8, 9, 7, 3, 4, 1, 2, 5},
		{7, 1, 3, 5, 2, 8, 6, 9, 4},
		{5, 4, 2, 9, 1, 6, 3, 7, 8},
	}

	notSolved2 := &[9][9]uint8{
		{8, 6, 4, 3, 7, 1, 2, 5, 9},
		{3, 2, 5, 8, 4, 9, 7, 6, 1},
		{9, 7, 1, 2, 6, 5, 8, 4, 3},
		{4, 3, 6, 1, 0, 2, 5, 8, 7},
		{1, 9, 8, 6, 5, 7, 4, 3, 2},
		{2, 5, 0, 4, 8, 3, 9, 1, 6},
		{6, 8, 9, 7, 3, 4, 1, 2, 5},
		{7, 1, 3, 5, 2, 8, 6, 9, 4},
		{5, 4, 2, 9, 1, 6, 3, 7, 8},
	}

	invalid := &[9][9]uint8{
		{8, 6, 4, 3, 7, 1, 2, 5, 9},
		{3, 2, 5, 8, 4, 9, 7, 6, 1},
		{9, 7, 1, 2, 6, 5, 8, 4, 3},
		{4, 3, 6, 1, 6, 2, 5, 8, 7},
		{1, 9, 8, 6, 5, 7, 4, 3, 2},
		{2, 5, 0, 4, 8, 3, 9, 1, 6},
		{6, 8, 9, 7, 3, 4, 1, 2, 5},
		{7, 1, 3, 5, 2, 8, 6, 9, 4},
		{5, 4, 2, 9, 1, 6, 3, 7, 8},
	}
	t.Run("when solved puzzle is given", func(t *testing.T) {

		got, err := Solve(solved)

		if err != nil {
			t.Error("Should not have returned a error", err)
		}

		for l := range got {
			if !sameArray(solved[l], got[l]) {
				t.Errorf("Lines are not equals wanted: %v got: %v", solved[l], got[l])
			}
		}
	})

	t.Run("when missing a number", func(t *testing.T) {

		got, err := Solve(notSolved1)
		if err != nil {
			t.Error("Should not have returned a error", err)
		}
		for l := range got {
			if !sameArray(solved[l], got[l]) {
				t.Errorf("Lines are not equals wanted: %v got: %v", solved[l], got[l])
			}
		}
	})

	t.Run("when missing two numbers", func(t *testing.T) {

		got, err := Solve(notSolved2)
		if err != nil {
			t.Error("Should not have returned a error", err)
		}
		for l := range got {
			if !sameArray(solved[l], got[l]) {
				t.Errorf("Lines are not equals wanted: %v got: %v", solved[l], got[l])
			}
		}
	})

	t.Run("when puzzle is invalid", func(t *testing.T) {

		_, err := Solve(invalid)
		if err == nil {
			t.Error("Should not have returned a error", err)
		}
	})

}
